CS253 web development Udacity
==================

Run in localHost
----------------

```python
dev_appserver.py [path]
```

Deploy to internet
----------------
```python
appcfg.py --oauth2 update [path]
```
